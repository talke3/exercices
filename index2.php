<?php
    include "Htmalpage.php";
?>    

<?php
    $fle = new Htmalpage('Example Title','Example Body');
    $fle->view ();

    $fle1 = new viewcolors('Example Title','Example Body');
    $fle1 -> color = 'red';
    $fle1->view ();
    $fle1 -> color = 'pink';
    $fle1->view ();
    $fle1 -> color = 'blue';
    $fle1->view ();
    $fle1 -> color = 'orange';
    $fle1->view ();
    echo '<br>';

    $fle2 = new fontsize('Example Title','Example Body'); 
    $fle2 -> color = 'orange';
    $fle2 -> size = "24px";
    $fle2->view();
    $fle2 -> color = 'pink';
    $fle2 -> size = "20px";
    $fle2->view();
    $fle2 -> color = 'blue';
    $fle2 -> size = "15px";
    $fle2->view();
    $fle2 -> color = 'red';
    $fle2 -> size = "10px";
    $fle2->view();
    $fle2 -> size = "25px";
    $fle2->view ();
    
?>