<?php
 class Htmalpage {
    protected $title = "Title";
    protected $body = "Body";
   
    function __construct($title = "",$body = ""){
           $this->title = $title;
           $this->body = $body;
    }
       
       
    function view (){
        echo 
        "<html>
        <head>
            <title>$this->title</title>
        </head>
        <body>
            $this->body
        </body>
        </html>
        ";
    }
} 


class viewcolors extends Htmalpage{
    protected $color;
    public function __set($property,$value){
        if ($property == 'color'){
             $color = array('red','pink','orange','blue');
             if(in_array($value, $color)){
                $this->color = $value; 
             }
             else{
                die ("Error Please enter a new color");        
             }
        }
    }
    public function view(){
        echo "<p style = 'color: $this->color'>
                <html>
                <head>
                    <title>$this->title</title> 
                </head>
                <body>
                    $this->body 
                </body>
                </html>    
                </p>"; 
    }      
} 

class fontsize extends viewcolors{
    protected $size;
    public function __set($property,$value){
        if ($property == 'size'){
            $size = array('10px','11px','12px','13px','14px','15px','16px','17px','18px','19px','20px','21px','22px','23px','24px');
            if(in_array($value, $size)){
                $this->size = $value;          
            }
            else{
                die("Error Please enter a new font size");        
            }
        }
        elseif($property == 'color'){
            parent::__set($property,$value);
        }
    }
     
    public function view(){
        echo "<html>
              <head>
                <title>$this->title</title> 
              </head>
              <body>
                <p style = 'color:$this->color; font-size:$this->size;' >$this->body</p>
              </body>
              </html>    
              </p>";       
    }            
}
?>